#include <AsyncEventSource.h>
#include <AsyncJson.h>
#include <WebHandlerImpl.h>
#include <ESPAsyncWebServer.h>
#include <WebAuthentication.h>
#include <AsyncWebSynchronization.h>
#include <AsyncWebSocket.h>
#include <WebResponseImpl.h>
#include <StringArray.h>

#include <pitches.h>
#include <Tone32.h>

#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <ChainableLED.h>

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif
#ifndef LED_BUILTIN
#define LED_BUILTIN 13
#endif

#define NUM_LEDS    1
#define CAPTOR_PIN  34
#define BUTTON_PIN  14

const unsigned char BUZZER_PIN = 4;
const unsigned char CHANNEL = 0;

const String pathServer = "http://192.168.1.19:8080";
const char* ssid = "Livebox-E7E6";
const char* password = "2227A3398404A1F03529E60E43";

int id = NULL;

String Etat = "";

// notes in the melody:
int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

boolean alarme = false;

TaskHandle_t xTaskCaptorHandle = NULL;

SemaphoreHandle_t mutex_v;

ChainableLED leds(26, 25, NUM_LEDS);

AsyncWebServer server(80);

void connectWifi();
void subscribe();
void unsubscribe();
void calculEtat(int valeur);
void updateEtat();
void TaskCaptor(void *pvParameters);
void TaskUpdate(void *pvParameters);
void TaskAlarme(void *pvParameters);
void IRAM_ATTR buttonPressed();
void startTaskCaptor();
void startTaskUpdate();
void startTaskAlarme();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Start");
  mutex_v = xSemaphoreCreateMutex(); 
  connectWifi();
  subscribe();
  pinMode(BUTTON_PIN, INPUT_PULLDOWN);
  attachInterrupt(BUTTON_PIN, buttonPressed, RISING);
  Etat = "MARCHE";
  startTaskCaptor();

  leds.setColorRGB(0, 0, 255, 0);

  AsyncCallbackJsonWebHandler* handler = new AsyncCallbackJsonWebHandler("/webhook/alarme/intrusion", [](AsyncWebServerRequest *request, JsonVariant& json) {
    JsonObject jsonObj = json.as<JsonObject>();
    if(jsonObj["alarmeId"].as<int>() == id && jsonObj["etat"].as<String>() != Etat) {
      Etat = jsonObj["etat"].as<String>();
      Serial.println("Webhook : " + Etat);
      if(Etat == "MARCHE") {
        leds.setColorRGB(0, 0, 255, 0);
        startTaskCaptor();
      } else {
        vTaskDelete(xTaskCaptorHandle);
        xTaskCaptorHandle = NULL;
        xSemaphoreGive(mutex_v);
        leds.setColorRGB(0, 0, 0, 0);
      }
    }
    request->send(200, "text/plain", "OK");
  });
  server.addHandler(handler);
  server.begin();
}

void startTaskCaptor()
{
  xSemaphoreTake(mutex_v, portMAX_DELAY);
  xTaskCreatePinnedToCore(TaskCaptor,"TaskCaptor",2000,NULL,9,&xTaskCaptorHandle,1);
}

void startTaskUpdate() {
  xTaskCreatePinnedToCore(TaskUpdate,"TaskUpdate",2000,NULL,9,NULL,1);
}

void startTaskAlarme() {
  xTaskCreatePinnedToCore(TaskAlarme,"TaskAlarme",2000,NULL,9,NULL,1);
}

void IRAM_ATTR buttonPressed()
{
    if(xTaskCaptorHandle == NULL){
      Etat = "MARCHE";
      leds.setColorRGB(0, 0, 255, 0);
      startTaskUpdate();
      startTaskCaptor();  
    } else {
      vTaskDelete(xTaskCaptorHandle);
      xTaskCaptorHandle = NULL;
      xSemaphoreGive(mutex_v);
      Etat = "ARRET";
      leds.setColorRGB(0, 0, 0, 0);
      startTaskUpdate();
    }
}

void connectWifi()
{
  WiFi.begin(ssid,password);

  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(100);
}

String httpGETRequest(const char* path)
{
  HTTPClient http;
  http.begin(path);

  int httpResponseCode = http.GET();
  String payload = "{}";

  if(httpResponseCode>0)
  {
    payload = http.getString();  
  }
  else{
    Serial.print(path);
    Serial.print(" error code :");
    Serial.println(httpResponseCode);
  }
  http.end();
  return payload; 
}

String httpPOSTRequest(const char* path, String dataRequest)
{
  HTTPClient http;

  http.begin(path);
  http.addHeader("Content-Type","application/json");
  int httpResponseCode = http.POST(dataRequest);
  String payload="{}";
  if(httpResponseCode>0)
  {
    payload = http.getString();  
  }
  else{
    Serial.print(path);
    Serial.print(" error code :");
    Serial.println(httpResponseCode);
  }
  http.end();
  return payload;
}

void subscribe()
{
  String path = pathServer + "/api/alarme/login";
  String response = httpPOSTRequest(path.c_str(), WiFi.localIP().toString());
  Serial.println(response);
  DynamicJsonDocument json(512);
  deserializeJson(json, response);
  id = json["id"];
  Serial.println("id = "+String(id));
}

void unsubscribe()
{
  String path = pathServer + "/api/alarme/unsubscribe/" + String(id);
  String response = httpGETRequest(path.c_str());
  id = NULL; 
}

void updateEtat()
{
  String path = pathServer + "/api/alarme/update";
  String dataPost = "{\"id\":" + String(id) + ",\"etat\":\"" + Etat + "\"}";
  String response = httpPOSTRequest(path.c_str(),dataPost);
  Serial.println(dataPost+" : "+response);
}

void calculEtat(int valeur)
{
  if(valeur >0 && Etat == "MARCHE")
  {
    Etat = "INTRU";
    leds.setColorRGB(0, 255, 0, 0);
    startTaskUpdate();
  }
}

void TaskCaptor(void *pvParameters)
{
  (void) pvParameters;

  for(;;)
  {
    int SensorValue = analogRead(CAPTOR_PIN);
    calculEtat(SensorValue);
    vTaskDelay(10);
  }
}

void TaskUpdate(void *pvParameters)
{
  (void) pvParameters;
  updateEtat();
  if(Etat == "INTRU" && alarme == false) {
    startTaskAlarme();
  }
  vTaskDelete(NULL);
}

void TaskAlarme(void *pvParameters)
{
  (void) pvParameters;
  int thisNote = 0;
  alarme = true;
  while(Etat == "INTRU"){
    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(BUZZER_PIN, melody[thisNote], noteDuration, CHANNEL);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    vTaskDelay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(BUZZER_PIN);
    thisNote = (thisNote + 1) % 8;
  }
  alarme = false;
  vTaskDelete(NULL);
}
